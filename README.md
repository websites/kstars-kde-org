# KStars Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kstars-kde-org)](https://binary-factory.kde.org/job/Website_kstars-kde-org/)

This is the git repository for [kstars.kde.org](https://kstars.kde.org), the website for KStars.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

#### For a new release
Update `data/releases.yaml`

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
