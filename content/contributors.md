---
title: Contributors
menu:
  main:
    parent: res
    weight: 7
---

{{% copyright-year %}}

KStars is distributed under the terms of the [GNU General Public License (GPL), Version 2](http://www.gnu.org/licenses/gpl.html).

**Original Authors:**
- Jason Harris \<kstars AT 30doradus DOT org\>, Original Author
- Jasem Mutlaq \<mutlaqja AT ikarustech DOT com\>, **Maintainer**, Major code contributions, INDI/Ekos

**Contributors:**
- Akarsh Simha \<akarsh DOT simha AT kdemail DOT net\>, Major Code contributions
- Hy Murveit \<hy AT murveit DOT com\>, Major Code contributions
- Eric Dejouhanet \<eric DOT dejouhanet AT gmail DOT com\>, Major Code contributions
- Wolfgang Reissenberger \<sterne-jaeger AT t-online DOT de\>, Major Code contributions
- Robert Lancaster, Major Code contributions
- James Bowlin \<bowlin AT mindspring DOT com\>, Major code contributions, reorganization of code, HTM
- Heiko Evermann \<heiko AT evermann DOT de\>, Major code contributions
- Thomas Kabelmann \<tk78 AT gmx DOT de\>, Major code contributions
- Pablo de Vicente \<pvicentea AT wanadoo DOT es\>, Major code contributions  
- Mark Hollomon \<mhh AT mindspring DOT com\>, Major code contributions  
- Carsten Niehaus \<cniehaus AT kde DOT org\>, Major code contributions
- Luciano Montanaro, Additional Data / Catalogs, Patches
- Glenn Becker, Additional Data / Catalogs
- Carl Knight \<sleepless DOT knight AT paradise DOT net DOT nz\>, Abell Planetary Catalog
- M&eacute;d&eacute;ric Boquien \<mboquien AT free DOT fr\>, Code contributions
- J&eacute;r&ocirc;me Sonrier \<jsid AT emor3j DOT fr DOT eu DOT org\>, Code contributions  
- Alexey Khudyakov \<alexey DOT skladnoy AT gmail DOT com\>, Code contributions
- Prakash Mohan \<prakash DOT mohan AT kdemail DOT net\>, The Observation Planner and Execute feature
- Henry de Valence \<hdevalence AT gmail DOT com\>, OpenGL painting
- Victor Carbune \<victor DOT carbune AT kdemail DOT net\>, SQLite backend for object data storage and retrieval, major contributions
- Samikshan Bairagya \<samikshan AT gmail DOT com\>, Supernovae in KStars
- Rafał Kułaga \<rl DOT kulaga AT gmail DOT com\>, **Maintainer**, Better printing support for finder charts
- Łukasz Jaskiewicz, Refactoring of OAL support

#### GSoC, SoCiS, SoK students

A listing of our Google Summer of Code, ESA SoCiS, and Season of KDE students:

- Artem Fedoskin (GSoC 2016): KStars Lite
- Cojocaru Raphael (GSoC 2016): KStars on Windows
- Akarsh Simha (GSoC 2008): 100 million stars
- Prakash Mohan (GSoC 2009): Observation planner
- Victor Cărbune (GSoC 2010): SQLite Database for Objects
- Henry de Valence (GSoC 2010): OpenGL rendering backend
- Rafał Kułaga (GSoC 2011): Professional finder chart printing
- Samikshan Bairagya (SoK 2011): Making KStars more suitable for a scientifically inclined user
- Łukasz Jaśkiewicz (ESA SoCiS 2011): Making KStars more useful for beginners

#### A round of applause for our successful Google Code-in students!

These pre-university students contributed to KStars as a part of
Google Code-in 2010.

- Cezar Mocan
- Ana-Maria Constantin
- Carl Gao
- Valery Kharitonov
- Diego Luca Candido
- Kristian Ivanov

#### Thanks for the patches and testing!

- Laurent Montel
- Eckhart W&ouml;rner
- Burkhard L&uuml;ck
- Nico Dietrich
- James Cameron
- Ralf Habacker
- Daniel Holler
- Patrick Spendrin
- Nuno Pinheiro
- Andrew Buck
- Patrice Levesque
- Douglas Phillipson
- Andrey Cherepanov
- Vipul Kumar Singh
- Jain Basil Aliyas
- Jeamy Lee
- Sivaramakrishnan S
- Adhiraj Alai
- Lukas Middendorf
- Sruthi Devi
- Keith Rusler
- Frederik Gladhorn
- Keerthi Kiran
- Jure Repinc
- Aditya Bhatt
- Alessio Sangalli

And everyone else who helped us by filing those bug reports, popularizing our software...
