---
title: 'Telescopes supported under KStars'
scssFiles:
- scss/tables.scss
menu:
  main:
    parent: indi
    weight: 1
faq:
- question: "When I try to Connect, KStars reports that the telescope is not connected to the serial/USB port. What can I do?"
  answer: |
    This message is triggered when KStars cannot communicate with the telescope. Here are few things you can do:
    - Check that you have both reading and writing permission for the port you are trying to connect to.
    - Check the connection cable, make sure it is in good condition and test it with other applications.
    - Check your telescope power, make sure the power is on and that the telescope is getting enough power.
    - Set the correct port in the INDI Control Panel under the Devices menu. The default device is /dev/ttyS0
    - Restart KStars and retry again.
- question: "Can I use KStars to do guiding?"
  answer: "If your telescope support Pulse commands, then KStars Ekos guiding module can autoguide your telescope using a CCD or a web cam."
- question: "KStars reports that the telescope is online and ready, but I cannot find the telescope's crosshair, where is it?"
  answer: "KStars retrieves the telescopes RA and DEC coordinates upon connection. If your alignment was performed correctly, then you should see the crosshair around your target in the Sky Map. However, the RA and DEC coordinates provided by the telescope may be incorrect (even below the horizon) and you need to Sync your telescope to your current target. You can use the right-click menu to center and track the telescope crosshair in the sky map."
- question: "The telescope is moving erratically or not moving at all. What can I do?"
  answer: |
    This behavior is mostly due to incorrect settings, please verify the following check list:
    1. Is the telescope aligned?
    1. Is the telescope alignment mode correct? Use INDI Control Panel to check and change these settings (Alt/Az,Polar, Land).
    1. Are the telescope's time and date settings correct?
    1. Are the telescope's longitude and latitude settings correct?
    1. Is the telescope's UTC offset correct?
    1. Are the telescope's RA and DEC axis locked firmly?
    1. Is the telescope's N/S switch (when applicable) setup correctly for your hemisphere?
    1. Is the cable between the telescope and computer in good condition?
---

### Overview

<img alt="Device Manager" src="device_manager.png" class="float-end">

**KStars** supports a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org). Using KStars Telescope Wizard, you can let KStars automatically scan ports for connected telescopes. Alternatively, you can start the driver for your telescope model in KStars Device Manager. Once a telescope is connected, you can command it either via INDI Control Panel, or directly form KStars SKy Map. Just right click on any object, and you will find your telescope listed in the popup menu. Using the context menu, you can issue commands such as *Slew*, *Track*, *Sync*, and *Park* in addition to motion commands. KStars assumes that you telescope is already powered, aligned, and ready for use. At your option, you can utilize Ekos *Align* module to help you in your polar alignment using the drift method. You can either control a telescope connected directly to your PC, or remotely. To connect to a remote telescope, add an INDI server host in the client tab of the device manager along with a port number. INDI server may run on many embedded devices, including Rasperry Pi.

### Telescopes

The following is a list of telescopes supported under KStars:

| Manufacturer | Model | Image | Driver | Support Status |
| ------------ | ----- | ----- | ------ | -------------- |
| Meade | LX200 Classic | <img src="lx200_classic.png"> | *indi_lx200_classic* | ✅ |
| Meade | LX200 Autostar, all models (LX200, LX800, ETX, etc.) | <img src="lx200_autostar.png"> | *indi_lx200_autostar, indi_lx200_gps* | ✅ |
| Celestron | Nexstar, all models. | <img src="nexstar.png"> | *indi_celestron_gps* | ✅ |
| Orion | Atlas/Siruis | <img src="orion.png"> | *indi_synscan* | ✅ |
| Takahashi | Temma | <img src="temma.png"> | *indi_temma* | ✅ |
| Astro-Physics | GTO V2 Mount | <img src="astrophysics.png"> | *indi_lx200ap* | ✅ |
| iOptoron | iEQ45 | <img src="ieq45.png"> | *indi_ieq45* | ✅ |
| SkyEng | SkyCommander | <img src="skycommander.png"> | *indi_skycommander* | ✅ |
| Custom | Argo Navis, Losmandy Gemini, Mel Bartels, EQ-6 MCU, HEQ-5/6 MCU | <img src="heq6.png"> | *indi_lx200_basic* | ✅ |

### FAQ

{{< faq name="faq" >}}
