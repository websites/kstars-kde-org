---
title: 'Filter wheels supported under KStars'
scssFiles:
- scss/tables.scss
menu:
  main:
    parent: indi
    weight: 4
faq:
- question: How can I download driver for my filter wheel?
  answer: |
    You can either:
    - Download the source archive (tar.gz) driver from [INDI's Sourceforge site](https://sourceforge.net/projects/indi/files/) and compile it yourself. This requires libindi development files.
    - Search for packages in your distribution software center or package manager.
    - If you run Ubuntu, you can download latest drivers directly from [Jasem Mutlaq's Personal Package Archive (PPA)](http://indilib.org/index.php?title=Download_INDI#Ubuntu).
---

### Overview

<img alt="Filter Selection" src="kstars_filter.png" class="float-end">

**KStars** supports a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org). By default, KStars ships drivers for Trutech filter wheel and filter simulator. Other filter wheel drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. Filter wheel may be either directly controlled from INDI Control Panel, or from Ekos Capture module as illustrated in the figure. Some filter driver do no support assigning names to each filter slot (e.g. Slot 0 Red, Slot 1 Blue..etc). Nevertheless, you can assign designated names to each filter wheel slot in KStars options.

### Filter Wheels

The following is a list of filter wheels supported under KStars:

| Manufaturer | Model | Image | Driver | Support Status |
| ----------- | ----- | ----- | ------ | -------------- |
| Quantum Scientific Imaging (QSI) | All models | <img src="qsi_wheel.png"> | *indi_qsi_ccd* | ✅ |
| Santa Barbra Instruments Group (SBIG) | All models | <img src="sbig_wheel.png"> | *indi_sbig_ccd* | ✅ |
| Starlight Xpress | All models | <img src="sx_wheel.png"> | *indi_sx_wheel* | ✅ |
| TruTechnology | All models | <img src="trutech_wheel.png"> | *indi_trutech_wheel* | ✅ |

### FAQ

{{< faq name="faq" >}}
