---
title: 'CCDs supported under KStars'
scssFiles:
- scss/tables.scss
menu:
  main:
    parent: indi
    weight: 2
faq:
- question: How can I download driver for my CCD?
  answer: |
    You can either:
    - Download the source archive (tar.gz) driver from [INDI's Sourceforge site](https://sourceforge.net/projects/indi/files/) and compile it yourself. This requires libindi development files.
    - Search for packages in your distribution software center or package manager.
    - If you run Ubuntu, you can download latest drivers directly from [Jasem Mutlaq's Personal Package Archive (PPA)](http://indilib.org/index.php?title=Download_INDI#Ubuntu).
---

### Overview

<img alt="FITS Viewer" src="fitsviewer.png" class="float-end">

**KStars** supports
a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org). By default, KStars only ships the CCD simulator driver, all other CCD drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. All CCD drivers (including webcams) send the image in FITS format, which are then displayed in KStars FITS Viewer tool. Using INDI control panel, you can capture images, set temperature, subframe, binning, and frame type (dark, light, bias..etc). To capture a sequence of images with filter wheel support, please use Ekos capture module included with KStars. The drivers can record in the FITS file the target's equatorial coordinates, filters used, JD, and more metadata. A CCD may be either designated as the primary CCD or a guider CCD in KStars Ekos tool. After an image is displayed in the FITS Viewer tool, you may perform some basic adjustments to the image including low pass and histogram equalization filters. Furthermore, KStars can detect and mark stars given that overall noise within an image is not too high. As with other INDI drivers, you may run a CCD driver either directly or remotely. For remote operations, compression of FITS data is recommended.

### CCDs

The following is a list of CCDs supported under KStars:

| Manufaturer | Model | Image | Driver | Support Status |
| ----------- | ----- | ----- | ------ | -------------- |
| Quantum Scientific Imaging (QSI) | All models | <img src="qsi.png"> | *indi_qsi_ccd* | ✅ |
| Santa Barbra Instruments Group (SBIG) | All models | <img src="sbig.png"> | *indi_sbig_ccd* | ✅ |
| QHY, Orion | QHY5, Orion Star Shoot Autoguider | <img src="qhy5.png"> | *indi_qhy_ccd* | ✅ |
| Starlight Xpress | All models, including Loadstar Autoguider. | <img src="sx.png"> | *indi_sx_ccd* | ✅ |
| Cannon | EOS DSLR | <img src="cannon.png"> | *indi_gphoto_ccd* | ✅ |
| Apogee | Alta-E & Alta-U | <img src="alta.png"> | *indi_apogeeu_ccd, indi_apogeee_ccd* | ✅ |
| Finger Lakes Instruments | All models | <img src="fli.png"> | *indi_fli_ccd* | ✅ |
| Webcams | Any camera supported by Video4Linux. KStars also support live video feed from webcams. | <img src="v4l.png"> | *indi_v4l_generic* | ✅ |

### FAQ

{{< faq name="faq" >}}
