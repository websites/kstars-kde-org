---
title: 'Other devices supported under KStars'
scssFiles:
- scss/tables.scss
menu:
  main:
    parent: indi
    weight: 5
---

### Overview

<img alt="Other Devices" src="other_devices.png" class="float-end">

**KStars** supports a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org). For devices that do not fall in the traditional categories, we list them here. They may include custom drivers for motion controllers, encoders, spectrometers, domes, weather stations...etc. If you are using or developing non-traditional INDI drivers, please let us know and we will add them here. Drivers should be not tightly coupled to a specific configuration of a device or setup in an observatory.

### Devices

The following is a list of other devices supported under KStars:

| Manufaturer | Model | Image | Driver | Support Status |
| ----------- | ----- | ----- | ------ | -------------- |
| Shoestring Astronomy | GPUSB | <img src="gpusb.png"> | *indi_gpusb* | ✅ |
| Diffraction Limited | MaxDome II : Observatory Dome Control System | <img src="maxdomeii.png"> | *indi_maxdomeii* | ✅ |
| Radio Astronomy Supplies | Spectracyber 1420Mhz hydrogen line spectrometer | <img src="spectracyber.png"> | *indi_spectracyber* | ✅ |
