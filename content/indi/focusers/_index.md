---
title: 'Focusers supported under KStars'
scssFiles:
- scss/tables.scss
menu:
  main:
    parent: indi
    weight: 3
faq:
- question: How can I download driver for my Focuser?
  answer: |
    You can either:
    - Download the source archive (tar.gz) driver from [INDI's Sourceforge site](https://sourceforge.net/projects/indi/files/) and compile it yourself. This requires libindi development files.
    - Search for packages in your distribution software center or package manager.
    - If you run Ubuntu, you can download latest drivers directly from [Jasem Mutlaq's Personal Package Archive (PPA)](http://indilib.org/index.php?title=Download_INDI#Ubuntu).
---

### Overview

<img alt="Focus Simulator" src="kstars_focus.png" class="float-end">

**KStars** supports a large number of astronomical instruments provided by the [INDI Library](http://www.indilib.org). By default, KStars ships the Meade, RoboFocus, Optec TCF-S, and focuser simulator drivers. Other focuser drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. Some focusers support absolute positioning, which makes them very useful for automatic focusing application. Autofocus is still possible with relative or focusers without positional feedback as the optimal focus point is determined by calculating the Half-Flux-Radius (HFR) of stars within an image. Using KStars Ekos tool, you can control the focuser in manual and automatic modes. The image must have a suffuciently good signal to noise ratio in order for KStars to detect stars and calculate their respective HFR. 

### Focusers

The following is a list of Focusers supported under KStars:

| Manufaturer | Model | Image | Driver | Support Status |
| ----------- | ----- | ----- | ------ | -------------- |
| Technical Innovations | RoboFocus | <img src="robofocus.png"> | *indi_robo_focus* | ✅ |
| Optec | TCF-S | <img src="tcfs.png"> | *indi_tcfs_focus, indi_tcfs3_focus* | ✅ |
| Finger Lakes Instruments | Precision Digital Focuser | <img src="fli_pdf.png"> | *indi_fli_focus* | ✅ |
| Meade | LX200GPS Microfocuser & 1206 Primary Mirror Focuser | <img src="meade_micro.png"> | *indi_lx200autostar* | ✅ |
| JMI | NGF Series & MOTOFOCUS | <img src="jmi_motofocus.png"> | *indi_lx200autostar* | ✅ |

### FAQ

{{< faq name="faq" >}}
