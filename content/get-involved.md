---
layout: get-involved
title: Get Involved
name: KStars
userbase: KStars
menu:
  main:
    weight: 4
---

Interested in KStars? Found KStars useful? Come by our [mailing list](https://mail.kde.org/mailman/listinfo/kstars-devel) and share your suggestions, and tell us what you liked / what you did not like. Any feedback and help will be appreciated.

If you know how to program, you're welcome to join our developer team! Get started by building [KStars from source](https://invent.kde.org/education/kstars/-/blob/master/README.md). The [community wiki](https://community.kde.org/Get_Involved/development) also provides resource about setting up your development environment. Most developer discussion happens on our [mailing list](https://mail.kde.org/mailman/listinfo/kstars-devel) or [issue tracker](https://invent.kde.org/education/kstars/-/issues), so if you need help, that's a good place to ask. KStars is written in C++ using [Qt](https://qt-project.org/) and [KDE](https://kde.org) libraries.

If you are an astronomy enthusiast, there is our [AstroInfo](/astroinfo) project.

## Some pointers to get started
- Join our [mailing list](https://mail.kde.org/mailman/listinfo/kstars-devel);
- Hang out on our [KStars Web Chat](https://webchat.kde.org/#/room/#kstars:kde.org) channel;
- Details on building KStars git master are [here](https://invent.kde.org/education/kstars/-/blob/master/README.md);
- Some junior and not-so-junior jobs [here](http://techbase.kde.org/Projects/Edu/KStars/JuniorJobs)!
- [Git repository](https://invent.kde.org/education/kstars) — hosts the latest code of KStars
- [KStars API](https://api.kde.org/kstars/html/index.html) — KStars API documentation
- [GSoC Guide](/gsoc) — Google Summer of Code guide for new prospective students.
