---
title: Features
menu:
  main:
    parent: res
    weight: 1
---

## Features organized by use-case
Pick your use case to see what KStars can do for you:
- {{< i18n "features.edu" "#features-for-educators" >}}
- {{< i18n "features.img" "#features-for-imaging--astrophotography" >}}
- {{< i18n "features.visual" "#features-for-visual-observation" >}}
- {{< i18n "features.any" "#features-for-any-astronomy-enthusiast" >}}

#### Features for Educators {#features-for-educators}
If you are an instructor, you could recommend KStars to your students for use as a learning aid. If you're a student, you can learn about the sky using KStars.

Here are some things you can do in KStars to explore astronomy and astrophysics:

- Change your geographic location to see how the view of the sky changes with latitude.
- Understand the difference between sidereal days and solar days by changing the speed of simulation.
- Attach trails to planets and set a high simulation rate to see their trajectories as they perform retrograde motion.
- When technical terms are underlined in blue, click on them to bring up an explanation from the included AstroInfo project.
- Explore coordinate systems by switching between equatorial and horizontal coordinates, and perform coordinate conversions in the KStars Astrocalculator.
- Learn about various topics in astronomy and astrophysics through the AstroInfo project (part of the KStars Handbook accessible through the Help menu).
- Set the time ahead by a couple 1000 years to see the effects of precession.
- Switch on and switch off gravitational lensing effects around the sun to understand Eddington's experiment.
- Switch to equatorial coordinates, set simulation rate to 1 year, double click on a star to keep it in focus, and zoom in to see the effects of aberration of light.
- Switch to equatorial coordinate, set simulation rate to 1 year, double click on an empty region, to see the effects of precession and nutation.
- Set the time to the distant future to see how the constellations change shape due to the proper motions of stars (prominent in Gemini, for example).
- Right click on any object and open the Details view to easily access lots of information, internet resources, images and professional surveys.
- Obtain sky survey images for any location from the right-click context menu.
- Control your observatory using KStars' [extensive support for astronomy equipment](/indi/).
- KStars speaks your language — you are very likely to find a translation / localization for your region.

#### Features for Imaging / Astrophotography {#features-for-imaging--astrophotography}
Whether you are at a professional observatory, or an amateur astrophotographer, KStars can help you image the sky. The Ekos imaging suite provides you with a smooth hassle-free workflow, removing the need to switch between multiple software tools for each part of the workflow.

- With extensive support for [INDI](/indi/), KStars can control almost any sort of astronomical equipment, including telescope mounts, motorized focusers, filter wheels and CCD and CMOS cameras.
- Use the FOV calculator to calculate the field-of-view of your imaging setup.
- [Ekos](https://indilib.org/about/ekos.html), KStars' astrophotography suite, provides a complete astrophotography workflow within KStars. Ekos can help with polar alignment, focusing, auto-guiding, and capture.
- The FITS Viewer lets you view FITS data files.
- Calculate conjunctions between celestial objects to find rendezvous that you can capture.
- KStars speaks your language — you are very likely to find a translation / localization for your region.

#### Features for Visual Observation {#features-for-visual-observation}
KStars has an extensive set of features that enhance your visual observation workflow. With the observation planner, all you need to do is add objects to your list. KStars takes care of ordering them in observing order, and with one click, lets you download DSS images for off-line comparison for observing sites where internet access is unavailable or poor. If you cannot take your laptop to your observing site, you can always export high-quality vector-graphic images of the sky that you can print for use as finder charts.

- With stars down to 16th magnitude, finding sufficient reference stars in the field is easy.
- If you want more than a complete NGC/IC catalog, you can add your own custom catalogs of objects.
- The powerful observation planner in KStars will help you plan your observing session, put objects in observation order, and cache DSS images for offline comparison with only a few clicks.
- You can also log your observations in KStars (this feature needs more polish to be fully usable, though.)
- KStars supports the OAL XML schema for observation logs, which makes these logs compatible with many software and log-sharing websites.
- Calculate your telescope's FOV for various eyepieces and save them in KStars, so you can quickly switch between FOV indicators with Page Up / Page Down keys.
- High quality finder charts allow you to do observation in places without electric power.
- Scripting support gives you the flexibility to write your own scripts to automate creation of finder charts etc.
- The experimental star-hopper feature automatically works out a star-hopping route from a star to a deep-sky object, given your field-of-view.
- The Jupiter's moons tool tells you the positions of Jupiter's Galilean moons, as is normally seen in astronomical ephemerides books.
- Configure KStars to alert you every time it learns of a new supernova in the sky!
- Predict conjunctions between celestial objects.
- Interface with XPlanet to render views of planets at any given time.
- KStars speaks your language — you are very likely to find a translation / localization for your region.

#### Features for any astronomy enthusiast! {#features-for-any-astronomy-enthusiast}
If you're new to astronomy, or are an enthusiast in general, KStars can be your guide as you explore the heavens. Already in the developers' version, KStars can auto-suggest objects for you to see in the night-sky.

- KStars renders an accurate representation of the sky, with the planets, Jupiter's moons, 100 million stars, 10000 deep-sky objects, comets and asteroids.
- Predict conjunctions between celestial objects.
- Learn more about astronomy and astrophysics through the helpful information links and the included AstroInfo project pages.
- (Coming very soon) Get predictions for interesting astronomical objects to observe with your equipment / sky conditions on any particular night.
- KStars speaks your language — you are very likely to find a translation / localization for your region.

## Full listing of important features

#### Features unique to KStars
Unique features in KStars not found in other free astronomy software (to the best of our knowledge):

- Fetch sky images from Digitized Sky Surveys, and the internet in general.
- Access information resources on sky objects from within KStars.
- Print high-quality, highly customizable, star charts with multiple charts at different zoom levels for each object.
- Full fledged astrophotography suite [Ekos](https://indilib.org/about/ekos.html) for a smooth imaging workflow.
- Plan and execute your observation sessions and save DSS images for offline use with the Observation Planner and "Execute" feature.
- Set up field of view symbols computed from your eyepiece and telescope focal length.
- Generate the positions of Jupiter's moons for the near future and near past and plot them as shown in professional astronomical ephemerides.
- Import your own custom catalogs from within KStars.
- Access some of KStars' internal calculations with the Astro-Calculator.
- Supernova alerts: KStars can automatically alert you about new supernovae at startup!
- Click on technical terms / astronomy jargon to open up an explanation from the included AstroInfo project.
- Use your favorite map projection method to render the celestial sphere on a flat screen.
- A very powerful tool to find conjunctions and oppositions (somewhat experimental).
- Compute star-hopping routes (experimental; incomplete but functional).
- Auto-suggest sky objects for beginners (coming soon).
- More translations and localization than most other software.

#### Catalogs
- Default catalog consisting of stars to magnitude 8
- Extra catalogs consisting of 100 million stars to magnitude 16
- Default deep-sky NGC / IC catalog (about 10000 objects)
- Download a more accurate revised NGC / IC catalog
- Downloadable catalogs including Messier Images, Abell Planetary Nebulae
- Add your own custom catalogs

#### Telescope / Equipment Control
- [Integration with INDI providing support for a wide range of astronomy instruments](/indi/)
- Full fledged astrophotography suite [Ekos](https://indilib.org/about/ekos.html) for a smooth imaging workflow

#### Other
- Corrections for precession, nutation and atmospheric refraction
- Tools for retrieval of data from Online Databases
- Scriptable actions using D-Bus
- Adjustable simulation speed in order to view phenomena that happen over long timescales
- Astroinfo project to help facilitate learning with the aid of KStars
- What's up tonight tool to see what objects are visible tonight
- Altitude vs Time tool to plot the time-variation in the altitude of an object (good to decide observation times)
- Sky Calendar tool to see how the planets move across the sky over a long duration of time
