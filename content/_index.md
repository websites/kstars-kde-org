---
title: KStars - Desktop Planetarium
features:
- desc: Graphical simulation of the sky with the planets, up to 100 million stars, 10000 deep-sky objects, comets and asteroids. Adjustable simulation rate.
  url: /img/screenshots/kstars_billionsandbillions.png
- desc: Access to several internet resources for information, imagery and data
  url: /img/screenshots/kstars_ghns.png
- desc: Complete astrophotography workflow
  url: /img/screenshots/kstars_ekos.png
- desc: A host of tools that predict conjunctions, plot time variation of positions of planets, perform calculations etc. 
  url: /img/screenshots/kstars_calculator.png
- desc: Powerful observation planner to plan your observations
  url: /img/screenshots/kstars_observationplanner.png
---
