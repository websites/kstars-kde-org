# Notes on making the Hugo site from the PHP site

- All pages are converted to Markdown (except `/todo`)
- Fix many outdated links
- Add a favicon
- `/indi/ekos` will be redirected when deployed on KDE server

## Notable pages that are kept

- `/catalogs` and its sub-pages
- `/indi` and its sub-pages: *I'm not sure these device lists are up-to-date*
- `/buzz`: I think it's a nice page to have, but **should it be kept?**
- `/todo`: **should it be kept?**

## Notable pages that are not kept

- Files in `/appstream` should be moved to cdn.kde.org/screenshots, and `/downloads`, to autoconfig.kde.org and cdn.kde.org. So they are not kept here
- Old pages are removed: `/embedded`, `/scripts`, `/news`, `/snapshot`, `/svn`, old screenshots and pictures, old "New Features" pages
- `/i18n`: links are added to `/get-involved` so this one isn't needed anymore
- `/history`: this page might be interesting, **should it be kept?**
